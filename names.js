'use strict';
/*
	Names:
		.camel(name) -> converts to camelName
		.capital(name) -> converts to CapitalName
		.dash(name) -> converts to dash-name
		.under(name) -> converts to under_name
		.short(name) -> converts to shortname
*/

var _ = require('underscore.string');

var Names = {

	// text utility SomeName -> someName
	camel: function(name) {
		return (name[0] || '').toLowerCase() + name.slice(1);
	},

	capital: function(name) {
		return _.capitalize(name);
	},

	// dash
	dash: function(name) {
		return _.dasherize(Names.camel(name));
	},

	// shortname
	short: function(name) {
		return Names.dash(name).replace(/\-/g,'');
	},

	// undername
	under: function(name) {
		return Names.dash(name).replace(/\-/g,'_');
	},

};

module.exports = Names;