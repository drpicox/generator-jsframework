'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var _ = require('underscore.string');
var tools = require('../tools.js');

var scopeRegex = /^(\w[\w\d]*)([=@&])(\w[\w\d]*)?$/;

var DirectiveGenerator = yeoman.generators.Base.extend({

	constructor: function() {
		yeoman.generators.Base.apply(this, arguments);

		this.argument('directive', { type: String, required: true });

		this.option('module', {
			alias: 'm',
			desc: 'module name',
			type: String,
		});

		this.option('scope', {
			alias: 's',
			desc: 'comma separated list of scopes, with @,=,& (note use \\& in your shell)',
			type: String,
		});

		this.option('watch', {
			alias: 'w',
			desc: 'comma separated list of watches of attributes',
			type: String,
		});

		this.option('view', {
			alias: 'v',
			desc: 'optional viewname, default directive name replacing Directive by View',
			type: String,
		});

		// a new template engine to remove non empty blank lines
		tools.wakePrettyEngine(this);
	},

	init: function () {

		this.app = this.config.get('app');
		this.module = this.options.module || this.directive.split('.').slice(0,-1).join('.');
		this.package = this.config.get('package')+'.'+ this.app + (this.module?'.'+this.module:'');
		this.directive = this.directive.split('.').slice(-1)[0];

		// parse options for scope
		this.hasScope = !!this.options.scope;
		if (typeof this.options.scope === 'string') {
			this.scopes = this.options.scope.split(',').map(function(scope) {
				var match = scope.match(scopeRegex);
				if (!match) {
					console.error('error: invalid scope "'+scope+'"');
					process.exit(1);
				}
				return {
					field: match[1],
					attribute: match[2] + (match[3] || ''),
				};
			});
		} else {
			this.scopes = [];
		}

		// parse watches
		if (typeof this.options.watch === 'string') {
			this.watches = this.options.watch.split(',');
		} else {
			this.watches = [];
		}

		// get view name
		if (typeof this.options.view === 'string') {
			this.view = this.options.view;
		} else {
			this.view = tools.changeEnd(this.directive, 'Directive', 'View');
		}
		this.templateUrl = tools.path(this, { file:this.view + '.html', folder:'templates' });
	},

	files: function () {
		var directiveFile = tools.path(this, { file:this.directive + '.js', folder:'directives' });
		var viewFile = tools.path(this, { file:this.view + '.html', folder:'views' });

	    this.template('_directive.js', directiveFile);
	    tools.injectScript(this, {script:directiveFile, name:this.directive});

	    this.template('_view.html', viewFile);
	},

});

module.exports = DirectiveGenerator;