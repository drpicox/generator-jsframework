'use strict';

// IE8 compatibility (only if restrict: contains 'E')
if (document) { document.createElement('<%= dash(view) %>'); }

angular.module('<%= package %>').directive('<%= camel(view) %>',
		[  function
		() {

	return {
		// It can be used as 'A' attribute or as 'E' element
		restrict: 'AE',

		<% if (hasScope) { %>
		// Variables to be used inside
		scope: {
			<% scopes.forEach(function(scope) { %>
			${scope.field}: '${scope.attribute}',
			<% }); %>
		},
		<% } %>

		// Template to render the content (https://docs.angularjs.org/guide/directive)
		templateUrl: '${templateUrl}',

		// Link function that provides scope utilities and element event binding
		link: function (scope<%= watches.length ? ', element, attrs' : '/*, element, watches*/' %>) {			

			<% if (watches.length) { %>
			// watches for attributes from the directive
			<%} watches.forEach(function(watch) { %>
			scope.$watch(attrs.<%= watch %>, function(/* newValue, oldValue */) {
				// update scope or whatever based in value
				
			});
			<% }) %>

			// some sample directive logic here
			scope.<%= camel(view) %> = '${directive}';
		},
	};

}]);
