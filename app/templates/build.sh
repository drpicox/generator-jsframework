#!/bin/bash

# Check if required tools are available (part 1/2)
if [ "$(which git)" = "" ]; then
	echo "$0: error: please install git to continue (http://git-scm.com/downloads)"
	exit 1
fi
if [ "$(which npm)" = "" ]; then 
	echo "$0: error: please install npm to continue (see http://nodejs.org/)"
	exit 1
fi

# Set up environment for proxy, if it is required
if nc -z dsgl-bcproxy 8080 >& /dev/null; then
	AUTH=SvcDsglMobile:DesigualMobile13
	export http_proxy=http://${AUTH}@dsgl-bcproxy:8080
	export HTTP_PROXY=http://${AUTH}@dsgl-bcproxy:8080
	export HTTPS_PROXY=http://${AUTH}@dsgl-bcproxy:8080
	export RSYNC_PROXY=${AUTH}@dsgl-bcproxy:8080
	npm config set proxy "$HTTP_PROXY"
	npm config set https-proxy "$HTTPS_PROXY"
	npm config set strict-ssl false
	npm config set registry "http://registry.npmjs.org/"
elif nc -w 5 -z www.google.com 80 >& /dev/null; then
	export http_proxy=
	export HTTP_PROXY=
	export HTTPS_PROXY=
	npm config delete proxy 
	npm config delete https-proxy 
	npm config delete strict-ssl
	npm config set registry "https://registry.npmjs.org/"
else
	echo "$0: warning: unknown proxy configuration (no internet connection?)"
fi

# Check if required tools are available (part 2/2)
if [ "$(which bower)" = "" ]; then 
	echo "$0: error: please install bower to continue (sudo npm install -g bower)"
	echo "$0: install bower now? (y/n)"
	read -t 30 q
	q=$(echo $q | cut -b 1)
	if [ "$q" = "y" -o "$q" = "Y" ]; then
		if sudo npm install -g bower; then
			echo "$0: log: bower appears to be installed"
			q=""
		else
			echo "$0: error: please try manually (sudo npm install -g bower)"
			exit 1
		fi
	else
		exit 1
	fi
fi
if [ "$(which grunt)" = "" ]; then 
	echo "$0: error: please install grunt-cli to continue (sudo npm install -g grunt-cli)"
	echo "$0: install grunt-cli now? (y/n)"
	read -t 30 q
	q=$(echo $q | cut -b 1)
	if [ "$q" = "y" -o "$q" = "Y" ]; then
		if sudo npm install -g grunt-cli; then
			echo "$0: log: grunt-cli appears to be installed"
			q=""
		else
			echo "$0: error: please try manually (sudo npm install -g grunt-cli)"
			exit 1
		fi
	else
		exit 1
	fi
fi

# Set ups the project and compiles it
echo "$0: log: npm install"   && npm   install && \
echo "$0: log: bower install" && bower install && \
echo "$0: log: grunt build"   && grunt build   && \
echo "$0: log: - successful - use 'grunt server:dist' to visualize result in local" && \
echo "$0: log: - successful - use 'grunt server' to debug with livereload"
