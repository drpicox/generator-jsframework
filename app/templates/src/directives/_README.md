Folder src/directives: Directives and Directives Provider
=========================================================

This is the directory for application directives.
Directives are extensions to the html which adds templates and javascript behaviour to them.
It is also useful to wrap non angular javascript plugins.


Framework create a new directive
--------------------------------

```shell
	$ fw directive YourDirective
```



Directive template
------------------

1. `src/directives/YourDirective.js`: create the controller logic

```javascript
	'use strict';

	// IE8 compatibility (TODO: only if restrict: contains 'E')
	if (document) { document.createElement('your-directive'); } // TODO put here your directive name

	angular.module('<%= package %>.<%= module %>').directive('yourDirective', // TODO put here your directive name
			[  function
			() {

		return {
			// It can be used as 'A' attribute
			// and as 'E' element
			restrict: 'AE',                      // TODO configure your directive restrictions

			// Variables to be used inside 
			// (automatically available in the template)
			// http://code.angularjs.org/1.2.3/docs/guide/directive#creating-custom-directives_demo_isolating-the-scope-of-a-directive
			scope: {
				'yourScopeA': '@Attribute',      // TODO your scope attribute values as string value (use {{}} for variables)
				'yourScopeE': '=Attribute',      // TODO your scope attribute values as reference (ex as used in ng-bind)
				'yourScopeF': '&Attribute',      // TODO your scope attributes for function calls (ex as used in ng-click)
			},

			// Template to render the content
			templateUrl: 'tpl/yourTemplate.html',        // TODO your template

			// Link function that provides scope utilities and element event binding
			link: function (scope, element, attrs) {
				
				// TODO watch for each attribute from directive which value is important
				scope.$watch(attrs.attributeName, function(value) {
		        	// update scope or whatever based in value
		      	});

		      	// TODO put your directive logic here
		      	scope.TODO = 'TODO';

		      	// TODO (if required) perform cleanup when the directive is removed
				element.on('$destroy', function() {
					// unregister dom events, ...
			    });
			},

			// TODO: optional, required directives, it modifies link function to include required directives scope
			require: '^parentDirective',

			// TODO: optional, add a controller for this directive
			controller: [ '$scope', /*more injections,*/ function ($scope) {
				// usually used to interact with other controllers
				// like having child directives that requires this
			}],

		};

	}]);
```

2. `src/index.html`: register the controller at minimized/app.js section

```html
	<!-- build:js(src) minimized/app.js -->
	<script src="<%= app %>.js"></script>
	...
	<script src="directives/YourDirective.js"></script> <!-- TODO: your controller file -->
	...
	<script src="<%= app %>Config.js"></script>
	<!-- endbuild -->
```

3. Use your directive at some html component:

	<div your-directive="some arguments">Some Content<div>



scope
-----

A directive (can, if defined scope:) have its own scope.
It works like see src/js/controllers/README.md $scope.


Transclude (intermedium)
----------

Optionally directives can transclude html code.
It is to include the directive html inside the template.


$compile (advanced)
--------

External html code (for example used in a non-angular plugin)
must be compiled in order to activate angular directives and controllers
(for example {{}} templates that can appear).

In order to obtain a element with angular enabled and be
append in the current dom, it must be compiled and associated to a $scope.

```javascript
	var innerElem = $compile("<p>{{value}}</p>")(scope);
	element.appendChild(innerElem);
```

May be you should be interested in use the `compile:` function instead of `link:`
in your directive definition. More [here](http://code.angularjs.org/1.2.8/docs/api/ng.$compile).


Directive Providers (more advanced)
-------------------

Additionally part of the logic of directives can be factorized (extracted common parts)
into a single resource. This will be presented as providers which will contain some of the common parts of html element transformation. By this way you can create generic directives and specific directives.

1. `src/services/YourGenericDirective.js`: before a the generic angular directive define the provider of the functionality

```javascript
    'use strict';

	// IE8 compatibility (TODO: only if restrict: contains 'E')
	if (document) { document.createElement('your-directive'); } // TODO put here your directive name

    angular.module('<%= package %>.<%= module %>')

    // Create the provider
    .provider('YourDirectiveProvider',  // TODO set your provider name here
            [ function    // TODO add here your dependences to be injected
            ( ) {         // TODO define here injected arguments 

        // TODO your defaults here
        var defaults = this.defaults = {
            config: 'defaultValue',
        };

        // here the function that instances this directive provider
        this.$get = 
        		[ function
        		( ) {
    
           function YourDirectiveProvider(element, config) { // TODO add correct arguments

           	   var options = this.options = angular.extend({}, defaults, config);

           	   // TODO here complete your provider
           	   this.destroy = function() {
           	   };
           }

           return YourDirectiveProvider;
        }];
    }])

    // Create the generic directive using this provider
	.directive('yourDirective', // TODO put here your directive name
			['YourDirectiveProvider', function
			( YourDirectiveProvider) {

		return {
			restrict: 'AE',        // TODO configure your directive restrictions

			// Link function that provides scope utilities and element event binding
			link: function (scope, element, attrs) {
				
		      	// TODO put your directive logic here with the provider
				provider = new YourDirectiveProvider(element, {});

		      	// TODO (if required) perform cleanup when the directive is removed
				element.on('$destroy', function() {
					// unregister dom events, ...
					provider.destroy();
			    });
			},

		};

	}]);

```


### Examples

- https://github.com/mgcrea/angular-strap/blob/master/src/tooltip/tooltip.js
- https://github.com/mgcrea/angular-strap/blob/master/src/typeahead/typeahead.js





Learn more
----------

Directive oficial docs can be found at:

- http://code.angularjs.org/1.2.8/docs/guide/controller
- http://code.angularjs.org/1.2.8/docs/api/ng.$compile
- http://code.angularjs.org/1.2.8/docs/api/ng.directive:ngTransclude

Directive providers:

- https://github.com/mgcrea/angular-strap/blob/master/src/tooltip/tooltip.js
- https://github.com/mgcrea/angular-strap/blob/master/src/typeahead/typeahead.js

