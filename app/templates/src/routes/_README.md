Folder src/js/routes: Routes
============================

This is the directory for application routes.
They load data and prepare views.



Framework create a new route
----------------------------

```shell
    $ fw route /new/route/:path
```



Route template
--------------

1. `src/routes/YourRoute.js`: create the handler controller logic
```javascript
    'use strict';

    angular.module('<%= package %>.<%= module %>').config(
            [ '$routeProvider', function
            (  $routeProvider ) {


        var YourItemResolver = // TODO one resolver for each resource
                ['YourService','$route','$location', function
                ( YourService , $route , $location ) {

            // TODO get url restlike params here
            var id = $route.current.params.id;

            // gets the current your
            var your = YourService.get(id);

            // return your
            return your;
        }];

        $routeProvider.when('/yours/:id', {  // TODO program your route
            
            templateUrl: 'views/yours.html', // TODO select correct view
            resolve: {
                your: YourItemResolver,    // TODO add resolvers (load data with promises)
            },

            controller: 'YourController',    // TODO select controller for this route/view
        });

    }]);
```

2. `src/index.html`: register the controller at minimized/app.js section
```html
    <!-- build:js(src) minimized/app.js -->
    <script src="<%= app %>.js"></script>
    ...
    <script src="routes/YourRoute.js"></script> <!-- TODO: your model file -->
    ...
    <script src="<%= app %>Config.js"></script>
    <!-- endbuild -->
```



Oficial docs
------------



