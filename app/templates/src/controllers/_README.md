Folder src/controllers: Controllers
===================================

This is the directory for application controllers.
This controllers can be applied to html elements 
or to routes.

Framework create a new controller
---------------------------------

```shell
	$ fw controller YourController
```



Controller template
-------------------

1. `src/controllers/YourController.js`: create the controller logic
```javascript
	'use strict';

	angular.module('<%= package %>.<%= module %>').controller('YourController', // TODO put here your controller name
			[ '$scope', function     // TODO add here your dependences to be injected
			(  $scope ) {            // TODO define here injected arguments 

		// TODO controller logic
		$scope.TODO = 'TODO';

	}]);
```

2. `src/index.html`: register the controller at minimized/app.js section
```html
	<!-- build:js(src) minimized/app.js -->
	<script src="<%= app %>.js"></script>
	...
	<script src="controllers/YourController.js"></script> <!-- TODO: your controller file -->
	...
	<script src="<%= app %>Config.js"></script>
	<!-- endbuild -->
```

3. Use it:

    a) Use your controler at some html component:
```html
	<div ng-controller="YourController">{{TODO}}<div>
```

    b) Use your controler inside a route:
    see: `src/routes/README.md`.
    In this case the route can define in the `resolve` section 
    new specific values to be injected.



$scope
------

The $scope is a specific injected value from angular
which represents the state and variables avilable inside
a html block.

If inside the controller logic we define:

```javascript
	$scope.contact = {
		name: 'John Smith',
		phone: '5553210',
		city: 'Oklahoma',
	};
```

we can use it inside the corresponding controller or template:

```html
	<div ng-controller="ExampleContactController">
		Name: {{contact.name}}<br>
		Phone: {{contact.phone}}<br>
		City: {{contact.city}}<br>
	<div>
```

or if it is a route, inside the route view template (at `src/views/example-contact.html`) like:

```html
	Name: {{contact.name}}<br>
	Phone: {{contact.phone}}<br>
	City: {{contact.city}}<br>
```

Scope values can also be used as arguments for directives:

```html
	<div example-contact-directive="contact"></div>
```


### Scope hirearchy

Scopes are organized in a hierarchy. 
In the current controller $scope is the variable environment
for the current part of html given that controller.

The idea is that each scope can see its own values
and parent values.

Childerns.
If inside the html there are other directives or controllers,
each directive and controller have their own scopes. 
These scopes has current scope as parent scope, if they
access to any variable not defined in their scope, they
will use current scope variables.

Sibilings.
It is possible to have many controllers and directives at the same level,
for example a html tag with many attributes each of them a directive.
All these scope will be sibilings.

Parent.
Childrens have access to parent scopes. All scopes but $rootScope always have
a $parentScope. 


Scope hierarchy variable access works as the Javascript prototype inheritance.



### Dot Notation

It is important to use **dot notation** if a scope value would be modified.
It ensures that the correct object is updated a no a new scope value is created.
It should be used like:

```html
	<input type="text" ng-model="contact.name">	
```

Because scopes are organized in a hirearchy if we access
to a variable dot field, the value of the field is updated
inside the variable, but, if we update the value of
the variable itself:

```html
	<input type="text" ng-model="wrong">	<!-- do not do that! always use "." to modify values -->
```

In this case if the variable ('`wrong`') was declared in a parent scope
a copy is saved in the current scope, so it create two variables in two different
scopes, probably what we want to be a single variable. 
So use dot notation.


Oficial docs
------------

Controller oficial docs can be found at:

- http://code.angularjs.org/1.2.8/docs/guide/controller
- http://code.angularjs.org/1.2.8/docs/api/angular.Module
- http://code.angularjs.org/1.2.8/docs/api/ng.$controllerProvider#register
- http://code.angularjs.org/1.2.8/docs/api/ng.$controller
