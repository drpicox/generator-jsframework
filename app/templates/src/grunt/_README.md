Folder src/grunt: grunt local project definitions
=================================================

In this folder you will find grunt auxiliary definitions for this project.
There you will find project specifics grunt specifications, so you should
not to modify Gruntfile.js.

    grunt /               --> this directory
      + copy.js           --> list of files that should be copied in the build
      + ports.js          --> default ports configuration
