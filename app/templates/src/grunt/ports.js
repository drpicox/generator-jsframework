// Ports used by grunt
module.exports = {

	connect:     <%= 9000 + portOffset %>,
	livereload: <%= 19000 + portOffset %>,

};

