Folder src/js/services: Services
================================

This is the directory for application services.
It is tight to the `$q` concept.


Framework create a new service
------------------------------

```shell
    $ fw service YourService
```


Valid service names should end with:
- Service: if it is a remote service
- Db: If it uses a local database
- Active: If it holds active/current data
- Factory: if it create other objects


Service template
----------------

1. `src/services/your.js`: create the handler controller logic
```javascript
    'use strict';

    angular.module('<%= package %>.<%= module %>').factory('YourService',  // TODO set your service name here
            ['$q', function    // TODO add here your dependences to be injected
            ( $q ) {           // TODO define here injected arguments 

        // TODO your service state here
        var yours = null;

        (function setup() {
            // TODO initialize your service state
            yours = 'TODO';
        })();

        var YourService = {
            delete: function () {
                // TODO implement delete
                return this;
            },
            get: function () {
                // TODO implement get
                return yours;
            },
            put: function (yours_new) {
                // TODO implement put
                yours = yours_new;
                return this;
            },
        };

        return YourService;

    }]);
```

2. `src/index.html`: register the controller at minimized/app.js section
```html
    <!-- build:js(src) minimized/app.js -->
    <script src="<%= app %>.js"></script>
    ...
    <script src="services/YourService.js"></script> <!-- TODO: your model file -->
    ...
    <script src="<%= app %>Config.js"></script>
    <!-- endbuild -->
```



Oficial docs
------------



