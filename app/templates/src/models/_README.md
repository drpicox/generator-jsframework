Folder src/js/models: Models
============================

This is the directory for application models.
Although models are not necessary to angular, 
they provide a way to import and make uniform data from many sources.

Models provides a new kind of injectable value
that can be used from other components. 
Models are a function contructor that must be instantiated with new.


Framework create a new model
----------------------------

```shell
	$ fw model YourModel
```



Model template
--------------

1. `src/models/YourModel.js`: create the handler controller logic
```javascript
	'use strict';

	angular.module('<%= package %>.<%= module %>').factory('YourModel', // TODO put here your model name
			[  function      // TODO add here your dependences to be injected
			() {             // TODO define here injected arguments 

		// model constructor
		return function (options) {
			// TODO put your logic here
			this.value = options.value;
		};
	}]);
```

2. `src/index.html`: register the controller at minimized/app.js section
```html
	<!-- build:js(src) minimized/app.js -->
	<script src="<%= app %>.js"></script>
	...
	<script src="models/YourModel.js"></script> <!-- TODO: your model file -->
	...
	<script src="<%= app %>Config.js"></script>
	<!-- endbuild -->
```

3. Inject it in your controllers, services, routes, or other models.
   (see examples below)



How to use
----------

Models can be injected in other components (controllers, directives, services, ...)
using the injection notation. 
Once they are injected they should have to be instantiated with new.
Example:

```javascript
	'use strict';
    
	angular.module('<%= package %>.<%= module %>').factory('YourService',
			['YourModel', function
			( YourModel ) {
     
		var instance = new YourModel({name: 'John', single: false});
     
		return {
			get: function () {
				return instance;
			},
		};
     
	}]);
```


Identity Map
------------

You are encouraged to use and identity map 
(a map/service that returns always the same model instance given an id)
so you can ensure propper data binding.

```javascript
   var map = [];

   function getOrCreate(id) {
      var model = map[id];
      if (!model) {
      	 model = new YourModel({id: id});
      	 map[id] = model;
      }
      return model;
   }
```


Preferred constructor arguments
-------------------------------

Although Javascript can have multiple arguments, 
many times some of them can be optional, 
and other times function call specification is more
clear to understand if we can explain which are each argument,
so it is recomended to use objects as constructors:

	var model = new YourModel({name:'John', age:30, single: true}); // GOOD

This is better than the alternative which could be:

	var model = new YourModel('John', 30, true); // XXX AVOID these

In that case it is not clear what are the 30 and the true value.



Arguments, exceptions and fastfail
----------------------------------

It is strongly recommended to check arguments. 
Although it has some performance impact, 
it will improve implementation stability and tracebility.

Arguments should be tested to be ok.
For example, if arguments name and single are mandatory,
and they are provided in the preferred constructor arguments
they can be checked as follows:

```javascript
	return function (options) {
		if (!angular.isObject(options) && !angular.isArray(options))  { throw 'illegal argument: options object expected';      }
		if (!angular.isString(options.name))                          { throw 'illegal options: name not defined as string';    }
		if (options.single !== true && options.single !== false)      { throw 'illegal options: single not defined as boolean'; }
    
		// logic stuff here
	};
```

Note: it should be improved to be removed dinamically in minimized code.


Oficial docs
------------

- http://code.angularjs.org/1.2.8/docs/api/AUTO.$provide#factory

