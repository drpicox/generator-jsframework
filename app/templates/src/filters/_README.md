Folder src/filters: Filters
===========================

This is the directory for application filters.
Filters are value transforms used directly in the html.
It transforms variables into other more complex.

Some of their uses is for text formating, like:

```html
	<span>{{importe | currency}}</span>
```

(in that case may be you are interested to use [i18n](http://code.angularjs.org/1.2.3/i18n/),
they are localization for filters like money), 
or may be you want to use them like filters for ngRepeat:

```html
	<tr ng-repeat="friend in friends | orderBy:name">
```



Framework create a new filter
-----------------------------

```shell
	$ fw filter FilterName
```



Filter template
----------------

1. `src/filters/YourFilter.js`: create the filter logic
```javascript
	'use strict';

	angular.module('<%= package %>.<%= module %>').filter('your', 
			[  function // TODO add here your dependences to be injected
			() {        // TODO define here injected arguments

		return function(value) {
			return value; // TODO transform value in some way
		}

	}]);
```

2. `src/index.html`: register the filter at minimized/app.js section
```html
	<!-- build:js(src) minimized/app.js -->
	<script src="<%= app %>.js"></script>
	...
	<script src="js/filters/YourFilter.js"></script> <!-- TODO: your filter file -->
	...
	<script src="<%= app %>Config.js"></script>
	<!-- endbuild -->
```

3. Use the filter logic in some html.
   (see above for examples)



Oficial docs
------------

- http://code.angularjs.org/1.2.3/docs/guide/filter
- http://code.angularjs.org/1.2.3/docs/api/ng.$filter
- http://code.angularjs.org/1.2.3/docs/api/ng.$filterProvider#register

