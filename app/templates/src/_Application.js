angular.module('<%= package %>.<%= app %>', [
						// Dependencies with other modules
	'ngRoute',			// angular std routes
	'ngTouch',			// angular std touch events (ng-...)
    'ngAnimate',		// angular std animations
	'ngSanitize',		// angular std html sanitization

	'mgcrea.ngStrap',   // bootstrap help, see http://mgcrea.github.io/angular-strap/ for more

									// Framework components
	//'jsfw.html5routehandler',		// activate html5 dependencies (deactivated by default)
	'jsfw.otherwisehomeroute',		// activate redirect to home
    'jsfw.splashscreendirective',	// activate splash screen directive

    /*  FRAMEWORK_INJECT(app.modules) */

]);