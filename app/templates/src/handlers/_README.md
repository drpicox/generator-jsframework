Folder src/js/handlers: Handlers
================================

This is the directory for application handlers.
They are a special kind of global controllers that
starts as soon as the application is ready.
They are useful to check routes errors, register platform specifics, and others.


Framework create a new handler
------------------------------

```shell
	$ fw handler YourHandler
```



Handler template
----------------

1. `src/handlers/YourHandler.js`: create the handler controller logic
```javascript
	'use strict';

	angular.module('<%= package %>.<%= module %>').run(
			[ '$rootScope', function  // TODO add here your dependences to be injected
			(  $rootScope ) {         // TODO define here injected arguments 


		// TODO put your logic here

	}]);
```

2. `src/index.html`: register the controller at minimized/app.js section
```html
	<!-- build:js(src) minimized/app.js -->
	<script src="<%= app %>.js"></script>
	...
	<script src="handlers/YourHandler.js"></script> <!-- TODO: your handler file -->
	...
	<script src="<%= app %>Config.js"></script>
	<!-- endbuild -->
```


$rootScope
----------

Scopes are structured in a tree, each scope has a parent and a children. 
When a value is requested, and the value is not in the current scope, it
looks for it in the `$parent` scope.

RootScope is the parent of all scope. If a scope value is not defined
in the current scope, it is searched in the parent. 

See `src/controllers/README.md` the $scope section.


Oficial docs
------------

- http://docs.angularjs.org/api/angular.Module

