// Safer js scripting
'use strict';

/* 
	Main documentation is mixed with code.

	Usange:
		- compile all: 
				$ grunt build

		- clean the www/ and intermediate files
				$ grunt clean:www

		- execute a web server with livereload (uncompressed for debug)
			(url: http://localhost:9000/index.html)
				$ grunt server

		- execute a web server with final result (compressed, no livereload)
			(url: http://localhost:9000/index.html)
				$ grunt server:dist


	Compilation directories:
		src/  -> sources
		www/  -> debug and compressed versions

	 Globbing
	 for performance reasons we're only matching one level down: */
	 // 'test/spec/{,*/}*.js' 
	 // use this if you want to recursively match all subfolders:
	 // 'test/spec/**/*.js'   
	 /*

 */

module.exports = function (grunt) {

	// Load project config
	var copy = require('./src/grunt/copy.js');
	var ports = require('./src/grunt/ports.js');

	// (L) Load here grunt plugins with tasks
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-rev');
	grunt.loadNpmTasks('grunt-svgmin');
	grunt.loadNpmTasks('grunt-usemin');
	grunt.loadNpmTasks('grunt-contrib-coffee');
	//grunt.loadNpmTasks('grunt-google-cdn'); //enable for web only developments
	grunt.loadNpmTasks('grunt-coffeelint');
	grunt.loadNpmTasks('grunt-angular-templates');
	require('time-grunt')(grunt);

	grunt.initConfig({

		// (C) Tasks configurations here

		// Processes less
		less:  {
			build: {
				files: {
					'.tmp/styles/styles.css': 'src/styles/styles.less'
				},
				options: {
					paths: [ 'src/styles', '.' ],
					compress: false,
					yuicompress: false,
					dumpLineNumbers: 'comments',
					optimization: 0,
				},
			},
		},

		// Adds -browser- specifics to css
		// available options are here: https://github.com/ai/autoprefixer
		autoprefixer: {
			options: ['last 2 version', '> 1%', 'ie 8', 'ie 7'],
			all: {
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			}
		},

		// Copies files to the www o .tmp folder
		copy: {
			options: {
				onlyIf: 'modified',
			},
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: 'src',
					dest: 'www',
					src: [
						'redirect.html',
						'*.{ico,png,txt}',
						'.htaccess',
						'*.xml',
						'images/{,*/}*.{gif,webp,webm}',
						'fonts/*',
						'assets/*',
						'resources/*',
					]
				}]
			},
			styles: {
				expand: true,
				cwd: 'src/styles',
				dest: '.tmp/styles',
				src: '{,*/}*.css'
			},
			index: {
				files: [
					{ src: 'src/index.html', dest: 'www/index.html'       },
				],
			},
			stub: {
				expand: true,
				cwd: 'src/stub',
				src: '{,*/}*',
				dest: 'www/stub',
			},
			copy: {
				src: copy,
				dest: 'www/'
			},
		},

		// ngtemplates to optimize and preload html templates
		ngtemplates: {
			app: {
				cwd: 'src',
				src: [ 'views/*.html' ],
				dest: 'www/minimized/views.js',
				options: {
					module: '<%= moduleName %>',
				},
			},
		},

		// JS hint checks js files looking for potential bugs
		// it gives hints to correct them and avoid problems
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'src/{,*/}*.js',
			]
		},

		// A task that minimizes png and jpg images
		// images are copied from the original src/ tot the minimized in www/
		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: 'src/images',
					src: '{,*/}*.{png,jpg,jpeg}',
					dest: 'www/images'
				}]
			}
		},

		// A task that minimizes svg images
		// images are copied from the original src/ to the minimized in www/
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: 'src/images',
					src: '{,*/}*.svg',
					dest: 'www/images'
				}]
			}
		},

		// Afegeix un hash del contingut als noms dels fitxers
		// evita que en la versió comprimida es repeteixin noms
		// es comunica amb 'usemin' per substituir els enllaços a index.html
		rev: {
			dist: {
				files: {
					src: [
						'www/minimized/*',
					]
				}
			}
		},
		
		// Usemin transforma index.html i les seves dependencies per comprimirles
		// pas previ que alerta a usemin que cal fer canvis
		// indicat a index.html en blocs de comentari <!-- build:XX(pwd) fitxer.XX -->
		// useminPrepare canvia la configuracio de les tasques
		// 'concat', 'cssmin', 'uglify' per a generar els fitxers necessaris
		useminPrepare: {
			html: 'src/index.html',
			options: {
				dest: 'www'
			}
		},

		// Usemin transforma index.html i les seves dependencies per comprimirles
		// en aquest pas fa les substitucions al fitxer index.html
		usemin: {
			html: [ 'www/index.html' ],
			//html: ['www/{,*/}*.html'],
			//css: ['www/css/{,*/}*.css'],
			options: {
				dirs: ['www']
			}
		},

		// Watches for changes 
		// this is the basic for livereload
		watch: {
			less: {
				files: ['src/styles/{,*/}*.less'],
				tasks: ['less','autoprefixer']
			},
			css: {
				files: ['src/styles/{,*/}*.css'],
				tasks: ['copy:styles','autoprefixer']
			},
			images: {
				files: ['src/images/{,*/}*'],
			},
			scripts: {
				files: ['src/{,*/}*.js'],
				tasks: ['jshint'],
			},
			stub: {
				files: ['src/stub/{,*/}*'],
			},
			views: {
				files: ['src/views/{,*/}*'],
			},
			livereload: {
				options: {
					livereload: ports.livereload,
				},
				files: [
					'src/assets/{,*/}*',
					'src/resources/{,*/}*',
					'src/images/{,*/}*',
					'src/views/{,*/}*',
					'src/{,*/,*/*/}*.js',
					'src/{,*/,*/*/}*.html',
					'.tmp/{,*/,*/*/}*',
				],
			}
		},

		// Creates a webserver
		connect: {
			options: {
				port: ports.connect,
				hostname: '0.0.0.0',
				open: 'http://localhost:'+ports.connect, //+'?livereload='+ports.livereload,
			},
			livereload: {
				options: {
					livereload: ports.livereload,
					base: [
						'www','.tmp','src','.'
					],
					//directory: 'www',
				}
			},
			dist: {
				options: {
					base: 'www'
				}
			}
		},

		clean: {
			www: {
				files: [{
					dot: true,
					src: [
						'www/*',
						'!www/.git*'
					]
				}]
			},
			tmp: {
				files: [{
					dot: true,
					src: [
						'.tmp/*',
						'!.tmp/.git*'
					]
				}]
			},
		},
		
		// Creates the documentation, a single html from all markdowns
		concat: {
			//markdown: {
			//  files: [{ src: [ 'README.md', 'src/**/*.md' ], dest: 'www/doc.md', }]
			//},
		},
		/*markdown: {
			root: {
				files: [
					{ src: 'www/doc.md', dest: 'www/doc.html', },
				],
			}
		},
		markdownpdf: {
			options: {
				concatFiles: true,
			},
			all: {
				files: {*/
		//      '.': [ 'README.md', 'src/**/*.md' ],
		//    }
		//  }
		//},

		// Speedups compilation performing tasks concurrently
		concurrent: {
			build: [
				'styles',
				'svgmin',
				'imagemin',
				'copy:index',
				'copy:stub',
				'ngtemplates',
				'copy:dist',
				'copy:copy',
			],
		},
		
	});

	// (T) Add here your task(s)

	// auxiliar task to do things in order
	grunt.registerTask('styles', [    // Compiles and copies css files
		'less:build',                   // - create css from less
		'copy:styles',                  // - copies css files
		'autoprefixer',                 // - adds prefixes to css (like -webkit-)
	]);

	// Executes a web server with or without livereload
	grunt.registerTask('server', function (target) {
		// without livereload
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		// with livereload
		grunt.task.run([
			'jshint',
			'clean:www',
			'clean:tmp',
			'styles',
			'connect:livereload',
			'copy:dist',
			'watch',
		]);
	});

	grunt.registerTask('build', [     // Builds the whole project
		'jshint',                       // - checks javascripts
		'clean:www',                    // - removes www/ directory
		'useminPrepare',                // - prepare usemin to process src/index.html
		'concurrent:build',             // - builds in parallel css,img,index,stub,tpl,views
		'concat',                       // - (usemin required) - concats files
		'cssmin',                       // - (usemin required) - minimizes csss    
		'uglify',                       // - (usemin required) - minimizes jss
		'rev',                          // - compute hashes for minimized resources file names
		'usemin',                       // - parses and replaces html instructions
		'clean:tmp',                    // - removes tempoaral .tmp/ directory
	]);

	grunt.registerTask('default', [
		'build'
	]);
};
