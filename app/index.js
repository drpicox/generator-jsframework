'use strict';


var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var JsframeworkGenerator = yeoman.generators.Base.extend({

	constructor: function() {
		yeoman.generators.Base.apply(this, arguments);

		this.argument('app', { 
			desc: 'name of the application in capitalized camel case, ex: MyApp',
			type: String,
			required: true,
		});

		this.option('package', {
			alias: 'p',
			desc: 'package name, ex: com.desigual',
			type: String,
			defaults: 'com.desigual',
		});
	},

	init: function() {

		// obtain app name and packageName
		this.package = this.args[0].split('.').slice(0,-1).join('.') || this.options.package;
		this.app     = this.app.split('.').slice(-1)[0];
		this.module  = this.app;

		// compute a random offset port for livereload and grunt
		this.portOffset = Math.floor((Math.random() * 1000));

		// save configured values for future
		this.config.set('app', this.app);
		this.config.set('package', this.package);
		this.config.set('module', this.module);

		// install dependences at the end (if not skipped)
		this.on('end', function () {
		  if (!this.options['skip-install']) {
			this.installDependencies();
		  }
		});
	},

	files: function() {
		// src folder
		this.mkdir('src');
		this.template('src/_index.html', 'src/index.html');

		// scripts
		this.template('src/_Application.js', 'src/'+this.app+'.js');
		this.template('src/_ApplicationConfig.js', 'src/'+this.app+'Config.js');

		this.mkdir('src/controllers');
		this.template('src/controllers/_README.md', 'src/controllers/README.md');
		this.mkdir('src/directives');
		this.template('src/directives/_README.md', 'src/directives/README.md');
		this.mkdir('src/filters');
		this.template('src/filters/_README.md', 'src/filters/README.md');
		this.mkdir('src/handlers');
		this.template('src/handlers/_README.md', 'src/handlers/README.md');
		this.mkdir('src/models');
		this.template('src/models/_README.md', 'src/models/README.md');
		this.mkdir('src/routes');
		this.template('src/routes/_README.md', 'src/routes/README.md');
		this.mkdir('src/services');
		this.template('src/services/_README.md', 'src/services/README.md');


		// resources
		this.mkdir('src/images');
		this.template('src/images/_README.md', 'src/images/README.md');
		this.mkdir('src/resources');
		this.template('src/resources/_README.md', 'src/README.md');
		this.mkdir('src/styles');
		this.template('src/styles/_README.md', 'src/styles/README.md');
		this.template('src/styles/_styles.less', 'src/styles/styles.less');
		this.mkdir('src/views');
		this.template('src/views/_README.md', 'src/views/README.md');


		// configuration
		this.mkdir('src/grunt');
		this.template('src/grunt/_README.md', 'src/grunt/README.md');
		this.copy('src/grunt/copy.js', 'src/grunt/copy.js');	
		this.copy('src/grunt/ports.js', 'src/grunt/ports.js');	

		this.template('_package.json', 'package.json');
		this.template('_bower.json', 'bower.json');	
		this.template('_README.md', 'README.md');	
		this.copy('Gruntfile.js', 'Gruntfile.js');	
		this.copy('build.sh', 'build.sh');
		this.copy('jshintrc', '.jshintrc');
		this.copy('gitignore', '.gitignore');
		this.copy('editorconfig', '.editorconfig');
	},
});


module.exports = JsframeworkGenerator;