'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var tools = require('../tools.js');

var folders = [
	'default',
	'app',
	'controllers',
	'directives',
	'handlers',
	'html',
	'images',
	'models',
	'modules',
	'routes',
	'scripts',
	'services',
	'styles',
	'resources',
	'views',
	'templates',
];

var ConfigGenerator = yeoman.generators.Base.extend({

	constructor: function() {
		yeoman.generators.Base.apply(this, arguments);

		var option = this.option;

		this.description = 'Shows current configuration if no options (except module) are present\n' +
		'otherwise configures files generation path or shows them.\n'+
		'Available variables:\n'+
		'      {{module|filtername}}  = module name to replace in the expression\n'+
		'      {{file|filtername}}    = file name to replace in the expression\n'+
		'      {{folder|filtername}}  = folder of resource to replace in the expression\n'+
		'      {{app|filtername}}     = app name to replace in the expression\n'+
		'Filtername is optional, legal values are camelname, capitalname, dashname, \n'+
		'undername, shortname.\n';

		this.option('module', { 
			desc: 'Which module has to be configured',
			type: String,
		});

		var generator = this;
		folders.forEach(function(folder) {
			generator.option(folder, {
				desc: 'Configures '+folder+' paths',
				type: String,
				defaults: true,
			});
		});
	},

	init: function() {
	},

	config: function() {
		var configured = false;
		var options = this.options;
		var config = this.config;
		var module = options.module || '';
		var paths = config.get('paths') || {};
		var templates = (paths && paths[module]) || {};

		folders.forEach(function(folder) {
			var value = options[folder];
			if (value === false) {
				delete templates[folder];
				configured = true;
			} else if (folderof value === 'string') {
				templates[folder] = value;
				configured = true;
			}
		});

		if (configured) {
			paths[module] = templates;
			config.set('paths', paths);
		} else {
			folders.forEach(function(folder) {
				var template = tools.pathTemplate({config:config,module:module,folder:folder});
				var inherited = !templates[folder];
				console.log(folder+(inherited?'~':'=')+template);
			});
		}
	},

});

module.exports = ConfigGenerator;