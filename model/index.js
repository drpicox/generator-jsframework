'use strict';
var _ = require('underscore.string');
var util = require('util');
var yeoman = require('yeoman-generator');
var tools = require('../tools.js');


var ModelGenerator = yeoman.generators.Base.extend({

	init: function() {
		// enforce one argument
		if (this.args.length != 1) {
			console.error('error: you should report your controller name (ex: MyModel)');
			process.exit(1);
		}

		this.subName = this.args[0];
		if (_.endsWith(this.subName, 'Model') && !this.options.model) {
			this.modelName = this.subName.slice(0, -'Model'.length);
		} else {
			this.modelName = this.options.model || this.subName;
		}
		this.moduleName = this.config.get('moduleName');
	},

	files: function() {
		var file = 'src/models/'+this.subName+'.js';

	    this.template('_model.js', file);
	    tools.injectScript(this, file, this.subName);
	},

});

module.exports = ModelGenerator;
