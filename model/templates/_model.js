'use strict';

/*  <%= modelName %>:
		.field			# TODO list your fields
		.methods(args)	# TODO list your methods
*/
angular.module('<%= moduleName %>').factory('<%= modelName %>',
		[ function
		( ) {

	// model constructor
	function <%= modelName %>(data) {
		angular.copy(data, this);
	}

	/*
	// TODO your model methods
	<%= modelName %>.prototype.method = function(args) {
		// TODO body...
	};
	*/

	return <%= modelName %>;
}]);
