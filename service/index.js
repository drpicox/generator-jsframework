'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var tools = require('../tools.js');


var ServiceGenerator = yeoman.generators.Base.extend({

	constructor: function() {
		yeoman.generators.Base.apply(this, arguments);

		this.argument('serviceName', { type: String, required: true });

		this.option('identity', {
			alias: 'i',
			desc: 'activates an identity map',
			type: String,
		});

		this.option('model', {
			alias: 'm',
			desc: 'which model is instantiated',
			type: String,
		});

		// a new template engine to remove non empty blank lines
		tools.wakePrettyEngine(this);
	},

	init: function() {

		// injectable variables
		// .serviceName
		// .serviceCamelname
		// .identity
		// .modelName
		// .instanceName
		// .instanceCamelname
		// .collectionCamelname


		// service name
		this.serviceCamelname = tools.camelname(this.serviceName);

		// identity (if applies)
		if (this.options.identity) {
			if (this.options.identity === true) {
				this.identity = 'id';
			} else {
				this.identity = this.options.identity;
			}
		} else {
			this.identity = false;
		}

		// model name (if applies)
		if (this.options.model) {
			if (this.options.model === true) {
				if (this.options.identity) {
					this.modelName =  tools.changeEnd(this.serviceName, 'sService', '');
				} else {
					this.modelName =  tools.changeEnd(this.serviceName, 'Service', '');
				}
			} else {
				this.modelName = this.options.model;
			}
		} else {
			this.modelName = false;
		}

		// instance and collection name
		if (this.modelName) {
			this.instanceName = tools.capitalname(this.modelName);
		} else {
			this.instanceName = tools.changeEnd(this.serviceName, 'Service', '');
			if (this.identity) {
				this.instanceName = tools.changeEnd(this.instanceName, 's', '');
			}
		}
		this.instanceCamelname = tools.camelname(this.instanceName);
		this.collectionCamelname = this.instanceCamelname + 's';

		// module name
		this.moduleName = this.config.get('moduleName');
	},

	files: function() {
		var file = 'src/services/'+this.serviceName+'.js';

	    this.template('_service.js', file);
	    tools.injectScript(this, file, this.serviceName);
	}
});

module.exports = ServiceGenerator;