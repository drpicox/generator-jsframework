'use strict';

angular.module('${moduleName}').factory('${serviceName}',
		[<% if (modelName) { %>'${modelName}', <%}%><% if (identity) { %>'$cacheFactory', <%}%>'$q', function
		(<% if (modelName) { %> ${modelName} , <%}%><% if (identity) { %> $cacheFactory , <%}%> $q ) {

	var <%= serviceName %> = {

		<% if (!identity) { %>
		delete: function () {
			return ${instanceCamelname};
		},
		<% } %>

		<% if (identity) { %>
		get: function (${identity}) {
			var ${instanceCamelname} = ${collectionCamelname}.get(${identity});
			if (!${instanceCamelname}) {
				<% if (modelName) { %>
				${instanceCamelname} = new ${modelName}({
					${identity}: ${identity},
				});
				<% } else { %>
				${instanceCamelname} = { ${identity}: ${identity} };
				<% } %>
				${collectionCamelname}.put(${identity}, ${instanceCamelname});
			}
			return $q.when(${instanceCamelname});
		},
		<% } else { %>
		get: function () {
			return ${instanceCamelname};
		},
		<% } %>

		<% if (identity) { %>
		peek: function (${identity}) {
			return ${collectionCamelname}.get(${identity});
		},
		<% } %>

		<% if (!identity) { %>
		put: function (new${instanceName}) {
			${instanceCamelname} = $q.when(new${instanceName});
			return ${instanceCamelname};
		},
		<% } %>
	};

	// your service private state here
	<% if (identity) { %>
	var ${collectionCamelname} = $cacheFactory('${moduleName}.${serviceName}');
	<% } else { %>
	var ${instanceCamelname};
	<% } %>


	// TODO private functions here


	// TODO initialize your service state
	(function setup() {
		<% if (!identity && !modelName) { %>
		${instanceCamelname} = $q.when('${serviceName}');
		<% } %>
		<% if (!identity && modelName) { %>
		${instanceCamelname} = $q.when(new ${modelName}());
		<% } %>
	})();


	return <%= serviceName %>;

}]);