'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var _ = require('underscore.string');
var tools = require('../tools.js');

var pathRegex = /^\/[\w\d-_:\/]*$/;
var partRegex = /:[\w][\w\d_]*/g;

var RouteGenerator = yeoman.generators.Base.extend({

	init: function () {
		// enforce one argument
		if (this.args.length != 1) {
			console.error('error: you should report your route path name (ex: /users/:user)');
			process.exit(1);
		}

		// enforce path
		if (!pathRegex.test(this.args[0])) {
			console.error('error: route path does not looks as a valid path (ex: /users/:user)');
			process.exit(1);
		}

		this.path = this.args[0];
		if (this.path === '/') {
			this.camelName = 'root';
		} else {
			this.camelName = _.camelize(this.path.replace(/\//g, ' ').replace(/:/g, ''));
		}
		this.subName = tools.capitalname(this.camelName);
		this.routeName = this.subName + 'Route';
		this.viewName = this.subName + 'View';
		this.controllerName = this.subName + 'Controller';		
		this.moduleName = this.config.get('moduleName');

		// deeper parts to ease generation
		this.parts = (this.path.match(partRegex) || []).map(function(index) {
			return {
				index: index.slice(1),
				service: index[1].toUpperCase() + index.slice(2) + 'sService',
			};
		});
	},

	files: function () {
		var file = 'src/routes/'+this.routeName+'.js';

	    this.template('_route.js', file);
	    tools.injectScript(this, file, this.routeName);

	    this.template('_view.html', 'src/views/'+this.viewName+'.html');

		this.invoke('jsframework:controller', {
			args: [ this.controllerName ], 
			options: { 
				injects: this.parts.map(function(part){return part.index;}).join(','),
			},
		});
	},

});

module.exports = RouteGenerator;
