'use strict';

// ROUTE <%= path %>
angular.module('<%= moduleName %>').config(
		[ '$routeProvider', function
		(  $routeProvider ) {

	<% parts.forEach(function(part) { %>
	var <%= part.service %>ItemResolver =
			['<%= part.service %>','$route', function
			( <%= part.service %> , $route ) {

		// get url restlike string param :<%= part.index %>
		var index = $route.current.params.<%= part.index %>;

		// gets the current 
		var <%= part.index %> = <%= part.service %>.get(index);

		// return it
		return <%= part.index %>;
	}];<% }) %>

	$routeProvider.when('<%= path %>', {
		
		// view template
		templateUrl: 'views/<%= viewName %>.html',

		// view required data
		resolve: {<% parts.forEach(function(part) { %>
			<%= part.index %>: <%= part.service %>ItemResolver,<% }); %>
		},

		// view controller
		controller: '<%= controllerName %>',
	});

}]);
