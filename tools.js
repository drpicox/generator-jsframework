'use strict';
/*
	Tools:
		.changeEnd(name, expected, replacement) -> if ends in expected, changes by replacement
*/

var fs = require('fs');
var _ = require('underscore.string');
var names = require('./names.js');


var lineWithBlanksRegex = /\n([ \t\r]+\n)+/g;
var doubleEmptyLineRegex = /\n\n+/g;

var scriptFolders = ['controllers','directives','handlers','filters','models','routes','services'];

// Common tools for all generators (up to now)
var Tools = {

	// change the end if matches
	changeEnd: function(name, expected, replace) {
		if (_.endsWith(name, expected)) {
			name = name.slice(0, -expected.length) + replace;
		}
		return name;
	},

	// injects a script into the src/index.html
	inject: function(generator,options) {
		var body, file, hasHook, hasSnippet, hook, lines, name, path, result, snippet, i, l;

		file      = options.file;        // target file where to inject
		hook      = options.hook;        // part to inject
		name      = options.name;        // name of the injected thing
		snippet   = options.snippet;     // snippet to inject

		path = generator.destinationRoot() + '/';
		hook = 'FRAMEWORK_INJECT('+hook+')';

		// read the file
		body = fs.readFileSync(path+file, 'utf8'), i, l;
		// check for the existence of the file
		if (body) {
			hasSnippet = body.indexOf(snippet) !== -1;
			hasHook = body.indexOf(hook) !== -1;

			if (hasSnippet) {
				result = 'identical';
			} else if (hasHook) {
				// split lines
				lines = body.split('\n');
				// search for the injection line
				for (i = 0, l = lines.length; i < l && lines[i].indexOf(hook) === -1; i++) {}
				// add the script tag before the injection line
				lines.splice(i, 0, '\t'+snippet);
				// save new lines
				body = lines.join('\n');
				fs.writeFileSync(path+file, body);
				result = 'create';
			} else {
				result = 'conflict';
			}
		} else {
			result = 'conflict';
		}

		switch (result) {
			case 'create':
				generator.log.create('registered "%s" in '+file, name); break;
			case 'identical':
				generator.log.identical('"%s" already registered in '+file, name); break;
			case 'conflict':
				generator.log.conflict('"%s" not registered in '+file, name); break;
		}
	},

	// injects a script into the src/index.html
	injectModule: function(options) {
		var file, generator, hook, module, packg, script, snippet;

		generator = options.generator;
		script    = options.script;      // script url to inject
		snippet   = options.snippet;     // snippet to inject
		hook      = options.hook;
		module    = options.module;
		packg     = options.package;        // name of the injected thing

		file = Tools.path({
			config: generator.config, module: module, folder: 'app'
		});
		hook = 'minimized/app.js';
		snippet = '\''+module+'\'';

		Tools.inject({
			file: file,
			generator: generator,
			hook: hook,
			name: name,
			snippet: snippet,
		});
	},

	// injects a script into the src/index.html
	injectScript: function(generator,options) {
		var file, hook, module, name, script, snippet;

		hook      = options.hook   || generator.hook || 'minimized/app.js';
		module    = options.module || generator.module;
		name      = options.name;        // name of the injected thing
		script    = options.script;      // script url to inject
		snippet   = options.snippet || '<script src="'+script+'"></script>';

		file = Tools.path(generator, { folder:'html' });
		Tools.inject(generator, {
			file: file,
			hook: hook,
			name: name,
			snippet: snippet,
		});
	},

	// generate a path 
	path: function(generator,options) {
		var app, config, file, folder, module, path, template;

		config = options.config || generator.config;
		app    = options.app    || generator.app     || config.get('app');
		file   = options.file;
		folder = options.folder;
		module = options.module || generator.module;

		module = module.replace(/\./g,'/');
		template = Tools.pathTemplate(generator,options);

		path = template.replace(/\{\{\s*module\s*(\|\s*(\w+))?\s*\}\}/g, function(match,p1,p2) {
			return p2 ? names[p2](module || '') : module;
		}).replace(/\{\{\s*file\s*(\|\s*(\w+))?\s*\}\}/g, function(match,p1,p2) {
			return p2 ? names[p2](file || '') : file;
		}).replace(/\{\{\s*folder\s*(\|\s*(\w+))?\s*\}\}/g, function(match,p1,p2) {
			return p2 ? names[p2](folder || '') : folder;
		}).replace(/\{\{\s*app\s*(\|\s*(\w+))?\s*\}\}/g, function(match,p1,p2) {
			return p2 ? names[p2](app || '') : app;
		});

		return path;
	},

	// compute path template
	pathTemplate: function(generator,options) {
		var config, isScriptfolder, module, paths, template, folder;

		config = options.config || generator.config;
		folder = options.folder;
		module = options.module || generator.module || '';

		paths  = config.get('paths') || {};
		isScriptfolder = scriptFolders.indexOf(folder) !== -1;

		// default templates
		switch (folder) {
			case 'app':
				template = 'src/{{app}}.js'; 
				break;
			case 'modules':
				template = 'src/{{module|short}}/{{module|capital}}.js';
				break;
			case 'html':
				template = 'src/index.html';
				break;
			case 'templates':
				template = './{{module|short}}/views/{{file}}';
				break;
			default:
				template = 'src/{{module}}/{{folder}}/{{file}}';
		}

		// template for main module
		template = (paths[''] && paths['']['default']) || template;
		if (isScriptfolder) {
			template = (paths[''] && paths['']['scripts']) || template;
		}
		template = (paths[''] && paths[''][folder]) || template;
		// template for module
		template = (paths[module] && paths[module]['default']) || template;
		if (isScriptfolder) {
			template = (paths[module] && paths[module]['scripts']) || template;
		}
		template = (paths[module] && paths[module][folder]) || template;

		return template;
	},

	// creates the generator template engine make pretty line verbosing
	wakePrettyEngine: function(generator) {
		// a new template engine to remove non empty blank lines
		var legacyEngine = generator.engine;
		generator.engine = function(body, data, options) {
			body = legacyEngine.call(generator, body, data, options);
			body = body.replace(lineWithBlanksRegex, '\n');
			body = body.replace(doubleEmptyLineRegex, '\n\n');
			return body;
		};

		for (var k in names) {
			generator[k] = names[k];
		}
	},


};


module.exports = Tools;
