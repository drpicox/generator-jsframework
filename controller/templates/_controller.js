'use strict';

angular.module('<%= package %>').controller('<%= controller %>',
		[<%= injects.map(function(i){return '\''+i+'\',';}).join('') %>'$scope', function
		(<%= injects.map(function(i){return '\ '+i+'\ ,';}).join('') %> $scope ) {

	<% injects.forEach(function(inject) { %>
	$scope.<%= inject %> = <%= inject %>;
	<% }); %>

	// TODO controller logic
	$scope.todo = 'TODO';
	$scope.action = function () {
		// TODO action...
	};

}]);
