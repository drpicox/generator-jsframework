'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var tools = require('../tools.js');


var ControllerGenerator = yeoman.generators.Base.extend({

	constructor: function() {
		yeoman.generators.Base.apply(this, arguments);

		this.argument('controller', { 
			desc: 'name of the controller in capitalized camel case, ex: MyController',
			type: String,
			required: true,
		});

		this.option('module', {
			alias: 'm',
			desc: 'module name',
			type: String,
		});

		this.option('injects', {
			alias: 'i',
			desc: 'a comma separated list of injectios for the controllers',
			type: String,
		});
	},

	init: function () {

		this.app = this.config.get('app');
		this.module = this.options.module || this.controller.split('.').slice(0,-1).join('.');
		this.package = this.config.get('package')+'.'+ this.app + (this.module?'.'+this.module:'');
		this.controller = this.controller.split('.').slice(-1)[0];

		if (this.options.module.length > 1) {
			this.package = this.package +'.'+ this.module;
			this.module = this.options.module;
		}

		if (this.options.injects) {
			this.injects = this.options.injects.split(',');
		} else {
			this.injects = [];
		}
	},

	files: function () {
		var file = tools.path(this, { file:this.controller + '.js', folder:'controllers' });

	    this.template('_controller.js', file);
	    tools.injectScript(this, {script:file, name:this.controller});
	},
});

module.exports = ControllerGenerator;