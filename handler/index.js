'use strict';
var util = require('util');
var yeoman = require('yeoman-generator');
var tools = require('../tools.js');


var HandlerGenerator = yeoman.generators.Base.extend({
	init: function () {
		// enforce one argument
		if (this.args.length != 1) {
			console.error('error: you should report your controller name (ex: MyHandler)');
			process.exit(1);
		}

		this.subName = this.args[0];
		this.moduleName = this.config.get('moduleName');
	},

	files: function () {
		var file = 'src/handlers/'+this.subName+'.js';

	    this.template('_handler.js', file);
	    tools.injectScript(this, file);
	},
});

module.exports = HandlerGenerator;